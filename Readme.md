## Guide to execute with kubernetes
In case of using a private docker images repository, use docker repository configutarion in kubernetes (in case it's configured in docker) using following script:
`cd kubernetes && bash create-secret.yaml`

Prepare kubernetes context if you need it:
```
kubectl get namespaces
kubectl config get-contexts
kubectl config set-context deephealth-bsc-context --namespace=compss-space
```

Or if you already have one you want to use:

`kubectl config use-context deephealth-bsc-context`

Prepare your image. Dockerfile is in docker folder. Execute Makefile in case you need to build and push to docker repository everytime. Be aware that "PREFIX" in Makefile sohuld be changed accordingly to the repository you are using to obtain the image.

If you want to deploy k8s overriding existing one, first delete with command (in kubernetes folder):

`kubectl delete -f compss_deephealth.yaml`

Modify the number of worker replicas in line 69 to adapt it to your needs.

Go to kubernetes folder and execute:

`kubectl create -f compss_deephealth.yaml`

Wait for the pods to be initialized.


Enter the master pod's container (named with prefix compss-rs-*****) like:

`kubectl exec -it compss-rs-*****  -- /bin/bash` (in case pod only has one container)

Run:

`bash configure_compss.sh`

Modify runcompss.sh to your needs, for example if it is desired to change the synchronization type just add `sync_type=0` for synchronous mode, 1 for asynchronous and 2 for fully asynchronous.

Run:

`bash runcompss.sh`


If you need any log from the pod, you can get them like:

`kubectl logs **POD NAME** -f`



