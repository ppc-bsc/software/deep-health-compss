
"""\
EDDL DISTRIBUTED API IMPLEMENTATION.
"""
from pycompss.api.api import compss_wait_on
from pycompss.api.api import compss_barrier

from eddl_array import paired_partition
from eddl_worker_distributed import *
from net_utils import net_aggregateParameters
from net_utils import net_aggregateResults
from net_utils import net_parametersToNumpy
from shuffle import block_shuffle_async, global_shuffle

from timeit import default_timer as timer


compss_object: Eddl_Compss_Distributed = None


def build(net, dataset, network, use_gpu, num_gpu):
    # Initialize the compss object
    global compss_object
    compss_object = Eddl_Compss_Distributed()

    # Define the computing service to use
    CS = eddl.CS_GPU(num_gpu) if use_gpu else eddl.CS_CPU()
    
    # Build the model in the master
    eddl.build(
        net,
        eddl.sgd(CVAR_SGD1, CVAR_SGD2),
        ["soft_cross_entropy"],
        ["categorical_accuracy"],
        CS,
        True
    )

    # Build the model in each distributed computing unit
    compss_object.build(dataset, network, use_gpu, num_gpu)

    # Wait until the models are created in each computing unit
    print("Building the model in distributed computing units...")
    compss_barrier()
    print("Building done!")


def fit_sync(model_params, x_train_dist, y_train_dist, num_workers, num_epochs, workers_batch_size, use_gpu, num_gpu):
    """
    Synchronization every epoch
    """
    print("Training epochs synchronously...")

    global compss_object

    # Define the number of images corresponding to each computing unit
    num_total_samples = x_train_dist.shape[0]
    num_images_per_worker = int(num_total_samples / num_workers)

    # Array to store the weights of each computing unit
    worker_params = [list() for i in range(0, num_workers)]
    
    # For every epoch taking into account parameter synchronization step
    for i in range(0, num_epochs):

        print("Training epoch: " + str(i+1))
        epoch_start_time = timer()

        # Shuffle dataset and train the epoch range
        x_train_dist, y_train_dist = global_shuffle(x_train_dist, y_train_dist)
        
        # x_train_dist is a dislib array already divided into the number of workers, so it is iterating over num_workers
        for j, (block_x, block_y) in enumerate(paired_partition(x_train_dist, y_train_dist)):
            worker_params[j] = compss_object.train_batch(
                                                    block_x,
                                                    block_y,
                                                    model_params,
                                                    num_images_per_worker,
                                                    workers_batch_size, use_gpu, num_gpu)            
        
        # Wait until every computing unit finishes its training (synchronous training)
        worker_params = compss_wait_on(worker_params)

        # Aggregate parameters
        model_params = net_aggregateParameters(worker_params)
        
        epoch_end_time = timer()
        epoch_final_time = epoch_end_time - epoch_start_time
        print("Elapsed time for epoch "+ str(i+1)+ ": " + str(round(epoch_final_time,2)) + " seconds")
    
    return model_params


def fit_async(model_params, x_train_dist, y_train_dist, num_workers, num_epochs, workers_batch_size, use_gpu, num_gpu):
    """
    Partial parameter aggregation after every worker completion
    """
    print("Training epochs asynchronously... (epochs information not printed)")

    global compss_object

    # Define the number of images corresponding to each computing unit
    num_total_samples = x_train_dist.shape[0]
    num_images_per_worker = int(num_total_samples / num_workers)

    # Define the parameters for each worker
    worker_params = [net_parametersToNumpy(model_params) for i in range(0, num_workers)]

    # Train and aggregate the parameters asynchronously for each distributed computing unit
    for i in range(0, num_epochs):
        
        # x_train_dist is a dislib array already divided into the number of workers, so it is iterating over num_workers
        for j, (block_x, block_y) in enumerate(paired_partition(x_train_dist, y_train_dist)):

            shuffled_x, shuffled_y = block_shuffle_async(block_x, block_y)
            block_x, block_y = [shuffled_x], [shuffled_y]

            worker_params[j] = compss_object.train_batch(
                                                    block_x,
                                                    block_y,
                                                    worker_params[j],
                                                    num_images_per_worker,
                                                    workers_batch_size, use_gpu, num_gpu)    
        
            # model_params is COMMUTATIVE therefore it is updating in each call
            worker_params[j] = compss_object.aggregate_parameters_async( model_params, worker_params[j], (1 / num_workers))

    # Wait until every computing unit has aggregated its parameters
    model_params = compss_wait_on(model_params)

    return model_params


def fit_full_async(model_params, x_train_dist, y_train_dist, num_workers, num_epochs, workers_batch_size, use_gpu, num_gpu):
    """
    Parameter aggregation at the end of num_epochs only
    """
    print("Training epochs fully asynchronous... (epochs information not printed)")

    global compss_object
    

    # Define the number of images corresponding to each computing unit
    num_total_samples = x_train_dist.shape[0]
    num_images_per_worker = int(num_total_samples / num_workers)

    # Array to store the weights of each computing unit
    worker_params = [net_parametersToNumpy(model_params) for i in range(0, num_workers)]

    # Shuffle the dataset
    x_train_dist, y_train_dist = global_shuffle(x_train_dist, y_train_dist)
    
    # For every epoch
    for i in range(0, num_epochs):
        
        # x_train_dist is a dislib array already divided into the number of workers, so it is iterating over num_workers
        for j, (block_x, block_y) in enumerate(paired_partition(x_train_dist, y_train_dist)):
            shuffled_x, shuffled_y = block_shuffle_async(block_x, block_y)
            block_x, block_y = [shuffled_x], [shuffled_y]
            worker_params[j] = compss_object.train_batch(
                                                    block_x,
                                                    block_y,
                                                    worker_params[j],
                                                    num_images_per_worker,
                                                    workers_batch_size, use_gpu, num_gpu)            
        
    
    # Wait until every computing unit finishes its training (synchronous training)
    worker_params = compss_wait_on(worker_params)

    # Aggregate parameters
    model_params = net_aggregateParameters(worker_params)
    
    return model_params
