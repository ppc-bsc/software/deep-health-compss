import numpy as np
import random

import pyeddl.eddl as eddl
from pycompss.api.constraint import constraint
from pycompss.api.parameter import *
from pycompss.api.task import task
from pyeddl.tensor import Tensor as eddlT

from cvars import *
from eddl_array import to_tensor
from net_utils import net_parametersToNumpy
from net_utils import net_parametersToTensor

from models import SIMPLE_MNIST, LeNet, VGG16


class Eddl_Compss_Distributed:

    def __init__(self):
        self.model = None

    @constraint(computing_units="${OMP_NUM_THREADS}")
    @task(dataset=IN,network=IN, use_gpu=IN, is_replicated=True)
    def build(self, dataset, network, use_gpu, num_gpu):

        # Dictionary relating the dataset with its number of classes and the first layer of the associated network
        dataset_network_dict = {"mnist": [10, {
                                "simple-mnist": eddl.Input([784])
                            }],
                            "cifar10": [10, {
                                "lenet": eddl.Input([3, 32, 32]),
                                "vgg16": eddl.Input([3, 32, 32])
                            }]
                            }

        # Dictionary relating the network argument with the function that implements it
        network_functions_dict = {"simple-mnist": SIMPLE_MNIST, "lenet": LeNet, "vgg16": VGG16}

        # Obtain the number of classes and the function that implements the network        
        num_classes = dataset_network_dict.get(dataset)[0]
        network_function = network_functions_dict.get(network)

        # Define the model
        in_ = dataset_network_dict.get(dataset)[1].get(network)
        out = network_function(in_, num_classes)
        net = eddl.Model([in_], [out])

        # Define the computing service to use
        CS = eddl.CS_GPU(num_gpu) if use_gpu else eddl.CS_CPU()

        # Build the model in this very node
        eddl.build(
            net,
            eddl.sgd(CVAR_SGD1, CVAR_SGD2),
            ["soft_cross_entropy"],
            ["categorical_accuracy"],
            CS,
            False
        )

        # Save the model. We have to serialize it to a string so COMPSs is able to serialize and deserialize from disk
        self.model = eddl.serialize_net_to_onnx_string(net, False)


    @constraint(computing_units="${OMP_NUM_THREADS}")
    @task(
        x_train={Type: COLLECTION_IN, Depth: 2},
        y_train={Type: COLLECTION_IN, Depth: 2},
        model_params=IN,
        num_images_per_worker=IN,
        workers_batch_size=IN,
        use_gpu=IN,
        target_direction=IN)
    def train_batch(self,
                    x_train,
                    y_train,
                    model_params,
                    num_images_per_worker,
                    workers_batch_size,
                    use_gpu, num_gpu):

        # Convert data to tensors
        x_train = to_tensor(x_train)
        y_train = to_tensor(y_train)

        # Deserialize from disk
        model = eddl.import_net_from_onnx_string(self.model)

        # Define the computing service to use
        CS = eddl.CS_GPU(num_gpu) if use_gpu else eddl.CS_CPU()

        # Build the model after deserializing and before injecting the parameters
        eddl.build(
            model,
            eddl.sgd(CVAR_SGD1, CVAR_SGD2),
            ["soft_cross_entropy"],
            ["categorical_accuracy"],
            CS,
            False
        )

        # Set the parameters sent from master to the model and reset loss
        eddl.set_parameters(model, net_parametersToTensor(model_params))
        eddl.reset_loss(model)        

        # Define the number of minibatches to compute
        num_mini_batches = int(num_images_per_worker / workers_batch_size)
        
        for j in range(num_mini_batches):
            # Select the mini-batch indices
            mini_batch_indices = list(range(j*workers_batch_size,(j+1)*workers_batch_size-1))
            eddl.train_batch(model, [x_train], [y_train], mini_batch_indices)
        
        eddl.print_loss(model, num_mini_batches)
        print("\nTrain batch individual completed in worker's train batch task\n")

        # Get parameters from the model and convert them to numpy so COMPSS can serialize them
        local_model_parameters = net_parametersToNumpy(eddl.get_parameters(model))

        return local_model_parameters


    @constraint(computing_units="${OMP_NUM_THREADS}")
    @task(model_params=COMMUTATIVE, parameters_to_aggregate=IN, mult_factor=IN, target_direction=IN)
    def aggregate_parameters_async(self, model_params, parameters_to_aggregate, mult_factor):

        for i in range(0, len(model_params)):
            for j in range(0, len(model_params[i])):
                model_params[i][j] = (
                            (model_params[i][j] + parameters_to_aggregate[i][j]) / 2).astype(np.float32)

        return model_params