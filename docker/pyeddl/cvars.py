import os

CVAR_SGD1 = 0.01
CVAR_SGD2 = 0.9
CVAR_CURRENT_PATH = os.getcwd()
CVAR_PARENT_PATH = os.path.abspath(os.path.join(CVAR_CURRENT_PATH, os.pardir))
CVAR_DATASET_PATH = os.path.join(CVAR_CURRENT_PATH, "datasets/")
