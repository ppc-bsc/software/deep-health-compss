"""\
SYNCHRONOUS TRAIN_BATCH IMPLEMENTATION.
"""
import argparse
from itertools import combinations_with_replacement
import sys
from timeit import default_timer as timer

import pyeddl.eddl as eddl
from pyeddl.tensor import Tensor as eddlT

import eddl_master_distributed_api as compss_api
from cvars import *
from eddl_array import array
from models import LeNet, VGG16, SIMPLE_MNIST
from net_utils import net_parametersToNumpy, net_parametersToTensor



def main(args):

    ##########################
    ##### INITIALIZATION #####
    ##########################

    # Process arguments
    num_workers = args.num_workers
    num_epochs = args.num_epochs
    workers_batch_size = args.workers_batch_size
    dataset = args.dataset
    network = args.network
    use_gpu = args.gpu
    num_gpu = [1 for i in range(args.num_gpu)]
    sync_type = args.sync_type

    # Define available datasets and network implementations
    dataset_list = ["mnist", "cifar10"]
    network_list = ["simple-mnist","lenet", "vgg16"]
    combination_list = [("mnist", "simple-mnist"),("cifar10", "lenet"),("cifar10", "vgg16")]

    # Dictionary relating the dataset with its number of classes and the first layer of the associated network
    dataset_network_dict = {"mnist": [10, {
                            "simple-mnist": eddl.Input([784])
                        }],
                        "cifar10": [10, {
                            "lenet": eddl.Input([3, 32, 32]),
                            "vgg16": eddl.Input([3, 32, 32])
                        }]
                        }
    # Dictionary relating the network argument with the function that implements it
    network_functions_dict = {"simple-mnist": SIMPLE_MNIST, "lenet": LeNet, "vgg16": VGG16}
    
    # Check that the dataset is downloaded, the network is implemented and the combination is valid
    if (dataset not in dataset_list):
        print("The required dataset is not available.")
    elif (network not in network_list):
        print("The required network is not available.")
    elif ((dataset, network) not in combination_list):
        print("The required dataset-network combination is not implemented.")

    else:

        # Obtain the number of classes and the function that implements the network
        num_classes = dataset_network_dict.get(dataset)[0]
        network_function = network_functions_dict.get(network)

        # Define the model
        in_ = dataset_network_dict.get(dataset)[1].get(network)
        out = network_function(in_, num_classes)
        net = eddl.Model([in_], [out])


        ##########################
        ##### MODEL BUILDING #####
        ##########################
        compss_api.build(net, dataset, network, use_gpu, num_gpu)
        eddl.summary(net)


        ###########################
        ##### DATASET LOADING #####
        ###########################
        x_train = eddlT.load(CVAR_DATASET_PATH + dataset+"_trX.bin")
        y_train = eddlT.load(CVAR_DATASET_PATH + dataset+"_trY.bin")
        x_test = eddlT.load(CVAR_DATASET_PATH + dataset+"_tsX.bin")
        y_test = eddlT.load(CVAR_DATASET_PATH + dataset+"_tsY.bin")

        # Normalize
        eddlT.div_(x_train, 255.0)
        eddlT.div_(x_test, 255.0)

        # Prepare data for distribution
        train_images_per_worker = int(eddlT.getShape(x_train)[0] / num_workers)
        x_train_dist = array(x_train, train_images_per_worker)
        y_train_dist = array(y_train, train_images_per_worker)


        ####################
        ##### TRAINING #####
        ####################
        start_time = timer()
        print("MODEL TRAINING...")
        print("Num workers: ", num_workers)
        print("Number of epochs: ", num_epochs)

        # Initial parameters that every computing unit will take in order to begin the training
        model_params = net_parametersToNumpy(eddl.get_parameters(net))

        if (sync_type == 0):
            model_params = compss_api.fit_sync(
                model_params,
                x_train_dist,
                y_train_dist,
                num_workers,
                num_epochs,
                workers_batch_size,
                use_gpu, num_gpu)

        elif (sync_type == 1):
            model_params =compss_api.fit_async(
                model_params,
                x_train_dist,
                y_train_dist,
                num_workers,
                num_epochs,
                workers_batch_size,
                use_gpu, num_gpu)

        elif (sync_type == 2):
            model_params =compss_api.fit_full_async(
                model_params,
                x_train_dist,
                y_train_dist,
                num_workers,
                num_epochs,
                workers_batch_size,
                use_gpu, num_gpu)
        else:
            print("No such sync type option available")

        # Set the parameters of the model to the aggregated parameters
        eddl.set_parameters(net, net_parametersToTensor(model_params))
            

        end_time = timer()
        final_time = end_time - start_time
        print("Total elapsed time: ", str(round(final_time,2)), " seconds")

        ######################
        ##### EVALUATION #####
        ######################
        print("Evaluating model against test set")
        eddl.evaluate(net, [x_test], [y_test])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--dataset", type=str, metavar="STR", default="mnist") # The dataset to work with
    parser.add_argument("--network", type=str, metavar="STR", default="simple-mnist") # The network that will be trained
    parser.add_argument("--num_workers", type=int, metavar="INT", default=4) # Number of computing units to divide the execution
    parser.add_argument("--num_epochs", type=int, metavar="INT", default=10) # Number of epochs to run the training
    parser.add_argument("--workers_batch_size", type=int, metavar="INT", default=250) # Size of each batch of the training phase
    parser.add_argument("--gpu", type=bool, metavar="BOOL", default=False) # True: Use GPU as CS --- False: Use CPU as CS
    parser.add_argument("--num_gpu", type=int, metavar="INT", default=1) # Number of GPUs per node
    parser.add_argument("--sync_type", type=int, metavar="INT", default=0) # 0: synchronous --- 1: asynchronous --- 2: fully asynchronous


    main(parser.parse_args(sys.argv[1:]))
