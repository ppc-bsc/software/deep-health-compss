#!/bin/bash
#Automatic Master configure
source get_pods_ip.sh
for Node in $Nodes;
do
  echo ${Node}
done

# PROJECT

echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Project>
    <MasterNode/>' > /root/project.xml
for Node in $Nodes;
do
        echo '<ComputeNode Name="'${Node}'">
        <InstallDir>/opt/COMPSs</InstallDir>
        <WorkingDir>/tmp/COMPSsWorker/</WorkingDir>
    </ComputeNode>' >> /root/project.xml
done
echo '</Project>' >> /root/project.xml



#RESOURCE

echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ResourcesList>' > /root/resources.xml
for Node in $Nodes;
do
        echo '<ComputeNode Name="'${Node}'">
        <Processor Name="MainProcessor">
            <ComputingUnits>4</ComputingUnits>
        </Processor>
        <Adaptors>
            <Adaptor Name="es.bsc.compss.nio.master.NIOAdaptor">
                <SubmissionSystem>
                    <Interactive/>
                </SubmissionSystem>
                <Ports>
                    <MinPort>43001</MinPort>
                    <MaxPort>43002</MaxPort>
                </Ports>
            </Adaptor>
        </Adaptors>
    </ComputeNode>' >> /root/resources.xml
done
echo '</ResourcesList>' >> /root/resources.xml

# #Execute COMPSs

# MASTER=\"$(kubectl get pods --selector=app=compss-master -o=custom-columns=:.status.podIP --no-headers)\"
# echo "MASTER IP ${MASTER}"
# runcompss -d --project=/root/project.xml --resources=/root/resources.xml --master_name=${MASTER} /root/cholesky.py 2 512 1