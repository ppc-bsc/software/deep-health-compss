#!/bin/bash
# Point to the internal API server hostname
APISERVER=https://kubernetes.default.svc

# Path to ServiceAccount token
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount

# Read this Pod's namespace
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)

# Read the ServiceAccount bearer token
TOKEN=$(cat ${SERVICEACCOUNT}/token)

# Reference the internal certificate authority (CA)
CACERT=${SERVICEACCOUNT}/ca.crt

echo "${API_SERVER}/api/v1/namespaces/${NAMESPACE}/endpoints"
# Extracting pods endpoints ip (all,master and workers)
Nodes=$(curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET \
  "${APISERVER}/api/v1/namespaces/${NAMESPACE}/endpoints/" | jq -rM ".items[].subsets[].addresses[].ip" | xargs echo)

echo "Pods IP's are:"
echo $Nodes
