#!/bin/bash
#SBATCH --job-name="sequential_cifar10_lenet"
#SBATCH --workdir=.
#SBATCH --output=exec_out/sequential_%j.out
#SBATCH --error=exec_out/sequential_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --time=00:60:00

#NUM_WORKERS=1
#SYNC_TYPE=0
#DATASET="cifar10"
#NETWORK="lenet"
#NUM_EPOCHS=10

#python3 -u eddl_master_train_batch.py --dataset=${DATASET} --network=${NETWORK} --num_workers=${NUM_WORKERS} --num_epochs=${NUM_EPOCHS} --sync_type=${SYNC_TYPE}  > exec_out/sequential_${DATASET}_${NETWORK}_sync_${SYNC_TYPE}_r1.out

DATASET="cifar10"
NETWORK="lenet"
NUM_EPOCHS=10
NUM_WORKERS=1
SYNC_TYPE=0
RUNS=1



BASE_FILE="/home/bsc37/bsc37726/projects/compss/exec_out/"
mkdir -p $BASE_FILE
export OMP_NUM_THREADS=48

for i in $( seq 1 ${RUNS} )
do	
	python3 -u \
	       eddl_master_train_batch.py --dataset=${DATASET} --network=${NETWORK} --num_epochs=${NUM_EPOCHS} \
	       	--num_workers=${NUM_WORKERS} --sync_type=${SYNC_TYPE} \
		> ${BASE_FILE}sequential_${DATASET}_${NETWORK}_sync_${SYNC_TYPE}_r${i}.out
done
