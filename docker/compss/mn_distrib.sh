#!/bin/bash -e
#SBATCH --ntasks=1
#SBATCH --error=exec_out/distrib_%j.err
#SBATCH --output=exec_out/distrib_%j.out

# Define application variables

exec_file=$(pwd)/eddl_master_train_batch.py
DATASET="cifar10"
NETWORK="lenet"
NUM_EPOCHS=10
RUNS=3
#qos_flag="--qos=debug"
qos_flag=""

CPUS_PER_NODE=48
WORKER_IN_MASTER_CPUS=0
execution_time=10 #in minutes
graph=$tracing

export OMP_NUM_THREADS=48

for w in 2
do
for s in 0 1 2
do
for i in $( seq 1 ${RUNS} )
do

NUM_WORKERS=${w}
SYNC_TYPE=${s}
num_nodes=`expr ${NUM_WORKERS} + 1`

# Enqueue job
enqueue_compss --sc_cfg=mn.cfg \
	--job_name="eddl_w${w}_s${s}_r${i}" \
	--num_nodes="${num_nodes}" \
        --cpus_per_node="${CPUS_PER_NODE}" \
        --cpus_per_task \
        --worker_in_master_cpus="${WORKER_IN_MASTER_CPUS}" \
	--exec_time="${execution_time}" \
	--scheduler=es.bsc.compss.scheduler.loadbalancing.LoadBalancingScheduler \
	--worker_working_dir=/home/bsc37/bsc37726/projects/compss/ \
	"${qos_flag}" \
	--lang=python \
	--python_interpreter="python3" \
	"$exec_file" --dataset=${DATASET} --network=${NETWORK} --num_workers=${NUM_WORKERS} --num_epochs=${NUM_EPOCHS} --sync_type=${SYNC_TYPE} > exec_out/distrib_${DATASET}_${NETWORK}_sync_${SYNC_TYPE}_workers_${NUM_WORKERS}.out
done
done
done
