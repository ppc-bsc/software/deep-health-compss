#!/bin/bash
#source get_pods_ip.sh
#masterIP=(${Nodes[2]})
echo "MasterIP is:" $MY_POD_IP
cd pyeddl


DATASET="cifar10"
NETWORK="lenet"
NUM_EPOCHS=10
NUM_WORKERS=1
SYNC_TYPE=0
NUM_GPU=1
RUNS=5



BASE_FILE="/root/exec_out/gpu${NUM_GPU}/"
mkdir -p $BASE_FILE

for i in $( seq 1 ${RUNS} )
do	
	conda run --no-capture-output -n pyeddl_pycompss_env python -u \
	       eddl_master_train_batch.py --dataset=${DATASET} --network=${NETWORK} --num_epochs=${NUM_EPOCHS} \
	       	--num_workers=${NUM_WORKERS} --sync_type=${SYNC_TYPE} \
		> ${BASE_FILE}sequential_${DATASET}_${NETWORK}_gpu_${NUM_GPU}_sync_${SYNC_TYPE}_r${i}.out
done

