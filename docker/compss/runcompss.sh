#!/bin/bash
# If you want to execute test python example
#runcompss --resources=/var/local/compss_conf/resources.xml --project=/var/local/compss_conf/project.xml --master_name=10.1.54-38 /tutorial_apps-stable/python/simple/src/simple.py 5
# If you want to execute pyddl in k8s
#source get_pods_ip.sh
#masterIP=(${Nodes[2]})
echo "MasterIP is:" $MY_POD_IP
cd pyeddl
conda run --no-capture-output -n pyeddl_pycompss_env runcompss --lang=python --python_interpreter=python3 --project=/root/project.xml --resources=/root/resources.xml --master_name=$MY_POD_IP eddl_master_train_batch.py --dataset="cifar10" --network="lenet" --num_epochs=10 --num_workers=4
