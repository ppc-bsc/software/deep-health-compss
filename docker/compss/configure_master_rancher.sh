#!/bin/bash
#Automatic Master configure

sleep 10
mkdir /root/.kube
touch /root/.kube/config
echo 'apiVersion: v1
kind: Config
clusters:
- name: "deephealth"
  cluster:
    server: "https://deephealth0/k8s/clusters/c-4mwwx"
    certificate-authority-data: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM3akNDQ\
      WRhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFvTVJJd0VBWURWUVFLRXdsMGFHVXQKY\
      21GdVkyZ3hFakFRQmdOVkJBTVRDV05oZEhSc1pTMWpZVEFlRncweE9URXlNVEV3T1RJeE16TmFGd\
      zB5T1RFeQpNRGd3T1RJeE16TmFNQ2d4RWpBUUJnTlZCQW9UQ1hSb1pTMXlZVzVqYURFU01CQUdBM\
      VVFQXhNSlkyRjBkR3hsCkxXTmhNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ\
      0tDQVFFQXhwSnNXeFZaelhnL05KNnMKNUJPRjZZVTdjQTZrQkEzMHhlZjlUNGZ0UWgvUUhsUXVFV\
      E9nbGJ5bERhMWM2ZitmVSszczBPOW9iZm5wRHhKMApVcGVUYkJxayttdkdPei8zM2I2SHN3UlV0K\
      1Z4bU1DcG1XZm8raUlQSkd6MEY5UDg0OWlmN1ZTRE90N2dCZlRYCkpia2lxby9VemQ1OENObGY4S\
      2k3T1RBQ1RoNW9MdGNhdWNQSzU1NnV1bmZjRzQ0em0raFBDRHVIMzVCcFNkdjIKSW1sbExTaTduO\
      WdlVzU5WWpUS2Rpd0Jsb1grL21yWmYwTTdOMnc3Kzc4ZVZhTzgxWkdITFM5ZDJZMzRlZU5sWAp6M\
      0lBOHZrdFVCZFdmdjNJelBqbXYzY3RSN3kzR3M2akhNTkR2aWNZc1RkVGtKVE9GM1BSSjRPK3lmU\
      1dYNFZuCkY4Q1p1d0lEQVFBQm95TXdJVEFPQmdOVkhROEJBZjhFQkFNQ0FxUXdEd1lEVlIwVEFRS\
      C9CQVV3QXdFQi96QU4KQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBaUVBUDlYc1UrRWZXSVJ6M290U\
      XRnamwrUm5xUmp3NFdDR1dWT2xwOQpCVVQzZkszdjc0U3RKb1lTM3VLSWtpSGt6d1BJb2krby9yQ\
      jFGWHdOQ0VKOWZ6c3d1TG1JMlV5anpRdExERFBMCnRNeWFYNm5uWWVyZVluMkg4STBsejVmRGpRM\
      GZjR0pHZDZMY1g1UHpTcmN3OWE0dE9BZnN2MStoYVhJcDNmWFQKWEJDMkZMYTVjRjdJNXhWZzc2Y\
      jdiNDduV0FPZHN5MVE1L0svOEJhb2Fkb052VW91QUk4Zjg2VG5rY2pjWDd5ZgpwYlJnNSsvOHJUd\
      1BHeFVkbUNrTjcreVFTcVp6S3Y5aG5FY212djh5amdXOW9LUkRtUEdtSk5WSXpBMWkwNlZtCkdQW\
      lIzNzFoNjl0ME5jR1E1Y2Q0ZFhRL2tTUkFSb1piVFlhZk4rZG14aS9MckE9PQotLS0tLUVORCBDR\
      VJUSUZJQ0FURS0tLS0t"
- name: "deephealth-deephealth0"
  cluster:
    server: "https://195.55.126.22:6443"
    certificate-authority-data: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUN3akNDQ\
      WFxZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFTTVJBd0RnWURWUVFERXdkcmRXSmwKT\
      FdOaE1CNFhEVEU1TVRJeE1UQTVNalF6TUZvWERUSTVNVEl3T0RBNU1qUXpNRm93RWpFUU1BNEdBM\
      VVFQXhNSAphM1ZpWlMxallUQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ\
      0VCQU52YjYrSFdvbUxWCnA0OVJvVHFjeU5YRk1sc1IzRzFjMVlBbDBZNUUrai9lTXROQkxBOUdZQ\
      ThCTTZBT3lScTlDS2traG5RNU80TGMKTjl1a0EvdkJCOGlKWXMrUHdwVzZlMTRNQnBSSW5lZWt0e\
      XZ3dUZjK0ZxRTlHOXVBRkx4cDY2a0tPV0pOZG1CdwpqYzdLTzRUS3JacWRFek5haklPcjBPWFhLV\
      VBVcUdzNWNSSnJwa0FOR2lTc0I2QjFwcEl3elB2OVJ6VlhtbmtqCmF2ZDlPWHUzNnVEMWgrZ0x1c\
      3BuaDYya2VlcXRrV2RPa0lQdnZiWFphcWdyclZpa1UxVzZZc2VUS3B3TjVvcHYKaWRVeXlkcHBLU\
      0FZSHcvRTVMSTJZQmZIdkpEMjNTY0Zickp6b3E3dTAyN2lNbjIvaXRhQ0pzL0hUdGN1OExhQgpmN\
      EhRdVpCWnpka0NBd0VBQWFNak1DRXdEZ1lEVlIwUEFRSC9CQVFEQWdLa01BOEdBMVVkRXdFQi93U\
      UZNQU1CCkFmOHdEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBQ2UvRVlDUWFMK3RJWC90ajIwT1FSe\
      XM0TDFSOGlueXI1emsKTDhMbmswS082UHlEUE15NnREL2JId09uQW5xMC85VFhaR1d2c010QnJmW\
      k85OGt0YWs0dmNmYnBOK3BDWS9XegpsK3ovejRpbkRia1dZb3NRdlVsQ0c0T1BnQUhOWmVCY3FoU\
      VptQW5aa0pTN1Y0NlU0VTI4UEhkOXFrRmRWZHBFCmNFNk0zc1lPQjJTYkE5MnpTVWhRL1hXS09qM\
      3E1VUwvWkFOSEpOVFRzTTdGSEhkMDFIYWp6ZVJPbTZ2ckxDcU8KWExOMXgyTHlrdjVGRGF3SEtOc\
      ThNL1dsblVpdFB4YndIWXB2RDBxaHFGTFBWR3NrU1Y0ZjZ0L1pkNXQrMVZBTwozT2JhTVVzdUNKd\
      C84dWwwcHkwckdzaG14cG15UGJDUmIzV2R1V2dKT2Qra1d1bFAybVk9Ci0tLS0tRU5EIENFUlRJR\
      klDQVRFLS0tLS0K"

users:
- name: "u-gtkzq"
  user:
    token: "kubeconfig-u-gtkzq.c-4mwwx:ql2m5zpjzkd8xpgqp9kh5qf52zxvkjwgnfg8mthfnm27kl2gqcglp2"

contexts:
- name: "deephealth"
  context:
    user: "u-gtkzq"
    cluster: "deephealth"
- name: "deephealth-deephealth0"
  context:
    user: "u-gtkzq"
    cluster: "deephealth-deephealth0"

current-context: "deephealth"' > /root/.kube/config

sleep 10
# PROJECT

echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Project>
    <MasterNode/>' > /root/project.xml
for Node in $(kubectl get pods --selector=app=compss-deploy -o=custom-columns=:.status.podIP --no-headers);
do
        echo '<ComputeNode Name="'${Node}'">
        <InstallDir>/opt/COMPSs</InstallDir>
        <WorkingDir>/tmp/COMPSsWorker/</WorkingDir>
        <User>root</User>
        <Application>
                <Pythonpath>/root/</Pythonpath>
        </Application>
    </ComputeNode>' >> /root/project.xml
done
echo '</Project>' >> /root/project.xml

#RESOURCE

echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ResourcesList>' > /root/resources.xml
for Node in $(kubectl get pods --selector=app=compss-deploy -o=custom-columns=:.status.podIP --no-headers);
do
        echo '<ComputeNode Name="'${Node}'">
        <Processor Name="MainProcessor">
            <ComputingUnits>4</ComputingUnits>
        </Processor>
        <Adaptors>
            <Adaptor Name="es.bsc.compss.nio.master.NIOAdaptor">
                <SubmissionSystem>
                    <Interactive/>
                </SubmissionSystem>
                <Ports>
                    <MinPort>43001</MinPort>
                    <MaxPort>43002</MaxPort>
                </Ports>
            </Adaptor>
        </Adaptors>
    </ComputeNode>' >> /root/resources.xml
done
echo '</ResourcesList>' >> /root/resources.xml

#Execute COMPSs

MASTER=\"$(kubectl get pods --selector=app=compss-master -o=custom-columns=:.status.podIP --no-headers)\"
echo "MASTER IP ${MASTER}"
runcompss -d --project=/root/project.xml --resources=/root/resources.xml --master_name=${MASTER} /root/cholesky.py 2 512 1